import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import es.ubu.tfg.activities.MainActivity;
import es.ubu.tfg.activities.MovieSummary;
import es.ubu.tfg.filmoteca.R;

@RunWith(RobolectricTestRunner.class)
public class MovieSummaryTest {

    private MovieSummary activity;

    @Test
    public void shouldHaveHappySmiles() throws Exception {
        String hello = new MainActivity().getResources().getString(R.string.hello_world);
        assertThat(hello, equalTo("Hola mundo!"));
    }

    @Before
    public void setup() {
        activity = Robolectric.buildActivity(MovieSummary.class)
                .create().get();
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull(activity);
    }
}
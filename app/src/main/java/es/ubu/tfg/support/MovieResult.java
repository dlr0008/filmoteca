package es.ubu.tfg.support;

/**
 * Clase de atributos de cada pelicula
 */
public class MovieResult {
    private final String backdropPath;
    private final String originalTitle;
    private final int id;
    private final String popularity;
    private final String posterPath;
    private final String releaseDate;
    private final String title;

    private MovieResult(Builder builder) {
        backdropPath = builder.backdropPath;
        originalTitle = builder.originalTitle;
        id = builder.id;
        popularity = builder.popularity;
        posterPath = builder.posterPath;
        releaseDate = builder.releaseDate;
        title = builder.title;
    }


    public static class Builder {
        private String backdropPath;
        private String originalTitle;
        private int id;
        private String popularity;
        private String posterPath;
        private String releaseDate;
        private String title;

        /**
         * Contrcutor
         * @param id
         * @param title
         */
        public Builder(int id, String title) {
            this.id = id;
            this.title = title;
        }

        /**
         * Setter del fondo
         * @param backdropPath
         * @return
         */
        public Builder setBackdropPath(String backdropPath) {
            this.backdropPath = backdropPath;
            return this;
        }

        /**
         * Setter del titulo
         * @param originalTitle
         * @return
         */
        public Builder setOriginalTitle(String originalTitle) {
            this.originalTitle = originalTitle;
            return this;
        }

        /**
         * Setter el id
         * @param id
         * @return
         */
        public Builder setId(int id) {
            this.id = id;
            return this;
        }

        /**
         * Setter de la popularidad
         * @param popularity
         * @return
         */
        public Builder setPopularity(String popularity) {
            this.popularity = popularity;
            return this;
        }

        /**
         * Setter del poster
         * @param posterPath
         * @return
         */
        public Builder setPosterPath(String posterPath) {
            this.posterPath = posterPath;
            return this;
        }

        /**
         * Setter de la fecha de lanzamiento
         * @param releaseDate
         * @return
         */
        public Builder setReleaseDate(String releaseDate) {
            this.releaseDate = releaseDate;
            return this;
        }

        /**
         * Setter del titulo
         * @param title
         * @return
         */
        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public MovieResult build() {

            return new MovieResult(this);
        }

    }

    public static Builder newBuilder(int id, String title) {

        return new Builder(id, title);
    }

    /**
     * Devuelve el fondo
     * @return fondo
     */
    public String getBackdropPath() {

        return backdropPath;
    }

    /**
     * Devuelve el titulo
     * @return titulo
     */
    public String getOriginalTitle() {

        return originalTitle;
    }

    /**
     * Devuelve el id
     * @return id
     */
    public int getId() {

        return id;
    }

    /**
     * Devuelve la popularidad
     * @return popularidad
     */
    public String getPopularity() {

        return popularity;
    }

    /**
     * Devuelve el poster
     * @return poster
     */
    public String getPosterPath() {

        return posterPath;
    }

    /**
     * Devuelve la fecha de lanzamiento
     * @return fecha
     */
    public String getReleaseDate() {

        return releaseDate;
    }

    /**
     * Devuelve el titulo
     * @return titulo
     */
    public String getTitle() {

        return title;
    }

    @Override
    public String toString() {

        return getTitle();
    }

}

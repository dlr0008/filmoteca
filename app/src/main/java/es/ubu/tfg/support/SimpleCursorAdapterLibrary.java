package es.ubu.tfg.support;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;


public class SimpleCursorAdapterLibrary extends ResourceCursorAdapter {

    protected int[] mFrom;
    protected int[] mTo;
    private CursorToStringConverter mCursorToStringConverter;
    private ViewBinder mViewBinder;
    String[] mOriginalFrom;


    public SimpleCursorAdapterLibrary(Context context, int layout, Cursor c, String[] from,
                                      int[] to, int flags) {
        super(context, layout, c, flags);
        mTo = to;
        mOriginalFrom = from;
        findColumns(c, from);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ViewBinder binder = mViewBinder;
        final int count = mTo.length;
        final int[] from = mFrom;
        final int[] to = mTo;

        for (int i = 0; i < count; i++) {
            final View v = view.findViewById(to[i]);
            String text = cursor.getString(from[i]);
            if (v instanceof TextView) {
                setViewText((TextView) v, text);
            } else{
                setViewImage((ImageView) v, text);
            }
        }
    }

    public void setViewImage(ImageView v, String value) {
        try {
            v.setImageResource(Integer.parseInt(value));
        } catch (NumberFormatException nfe) {
            v.setImageURI(Uri.parse(value));
        }
    }


    public void setViewText(TextView v, String text) {
        v.setTextColor(Color.BLACK);
        v.setText(text);
    }

    @Override
    public CharSequence convertToString(Cursor cursor) {
        int mStringConversionColumn = -1;
        if (mCursorToStringConverter != null) {
            return mCursorToStringConverter.convertToString(cursor);
        }

        return super.convertToString(cursor);
    }


    private void findColumns(Cursor c, String[] from) {
        if (c != null) {
            int i;
            int count = from.length;
            if (mFrom == null || mFrom.length != count) {
                mFrom = new int[count];
            }
            for (i = 0; i < count; i++) {
                mFrom[i] = c.getColumnIndexOrThrow(from[i]);
            }
        } else {
            mFrom = null;
        }
    }

    @Override
    public Cursor swapCursor(Cursor c) {
        // super.swapCursor() will notify observers before we have
        // a valid mapping, make sure we have a mapping before this
        // happens
        findColumns(c, mOriginalFrom);
        return super.swapCursor(c);
    }

    public static interface ViewBinder {
        boolean setViewValue(View view, Cursor cursor, int columnIndex);
    }


    public static interface CursorToStringConverter {
        CharSequence convertToString(Cursor cursor);
    }
}
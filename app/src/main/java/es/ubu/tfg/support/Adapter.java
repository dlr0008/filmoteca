package es.ubu.tfg.support;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import es.ubu.tfg.activities.About;
import es.ubu.tfg.activities.Add;
import es.ubu.tfg.activities.Explore;
import es.ubu.tfg.activities.Library;
import es.ubu.tfg.activities.Settings;
import es.ubu.tfg.filmoteca.R;

/**
 * Clase que adapta nuestro fragment(Menu Lateral) y lo hace funcional
 */
public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {
    List<Information> data= Collections.emptyList();
    private LayoutInflater inflater;

    /**
     * Construcutor
     * @param context contexto de la aplicacion
     * @param data lista de resultados
     */
    public Adapter(Context context, List<Information> data){
        inflater=LayoutInflater.from(context);
        this.data=data;
    }

    /**
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.fragm_list_item, parent,false);
        return new MyViewHolder(view);
    }

    /**
     * Asiganmos titulo e icono
     * @param holder holder
     * @param position posicion de cada elemento
     */
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Information current=data.get(position);
        holder.title.setText(current.title);
        holder.icon.setImageResource(current.iconId);
    }

    /**
     * Tamaño total
     * @return tamaño
     */
    @Override
    public int getItemCount() {
        return data.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView title;
        ImageView icon;

        /**
         * Contructor
         * @param itemView
         */
        public MyViewHolder(View itemView) {
            super(itemView);
            title= (TextView) itemView.findViewById(R.id.listText);
            icon= (ImageView) itemView.findViewById(R.id.listIcon);
            itemView.setOnClickListener(this);
        }

        /**
         * Accion que hacemos cuando se pulsa un elemento
         * @param v view
         */
        @Override
        public void onClick(View v) {
            if(getPosition()==0){
                Intent myIntent = new Intent(v.getContext(), Add.class);
                v.getContext().startActivity(myIntent);
            } else if (getPosition() == 1) {
                Intent myIntent = new Intent(v.getContext(), Library.class);
                final ProgressDialog progressDialog;
                progressDialog = new ProgressDialog(v.getContext());
                progressDialog.setMessage("Cargando");
                progressDialog.setCancelable(true);
                progressDialog.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                }).start();
                v.getContext().startActivity(myIntent);
            } else if (getPosition() == 2) {
                Intent myIntent = new Intent(v.getContext(), Explore.class);
                v.getContext().startActivity(myIntent);
            } else if (getPosition() == 4) {
                Intent myIntent = new Intent(v.getContext(), Settings.class);
                v.getContext().startActivity(myIntent);
            } else if (getPosition() == 5) {
                Intent myIntent = new Intent(v.getContext(), About.class);
                v.getContext().startActivity(myIntent);
            }
        }
    }
}
package es.ubu.tfg.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Clase para manejar la base de datos(insert, delete, etc..)
 */
public class DbManager {

    public static final String TABLE_NAME="movies";
    public static final String CN_ID="_id";
    public static final String CN_TITLE="title";
    public static final String CN_DATE="date";
    public static final String CN_POPULARITY="popularity";
    public static final String CN_POSTER="poster";
    public static final String CN_POSTER_CACHE="posterCache";
    public static final String CN_BACKDROP="backdrop";
    public static final String CN_OWNTITLE="ownTitle";
    public static final String CN_DIRECTOR="director";
    public static final String CN_CASTING="casting";
    public static final String CN_DURATION ="duration";
    public static final String CN_ALBUM ="album";

    //CREATE TABLE
    public static final String CREATE_TABLE ="create table " + TABLE_NAME + " ("
            + CN_ID + " integer primary key autoincrement,"
            + CN_TITLE + " text not null,"
            + CN_DATE + " text,"
            + CN_POPULARITY + " text,"
            + CN_POSTER + " text,"
            + CN_POSTER_CACHE + " text,"
            + CN_BACKDROP + " text,"
            + CN_OWNTITLE + " text,"
            + CN_DIRECTOR + " text,"
            + CN_CASTING + " text,"
            + CN_DURATION + " text,"
            + CN_ALBUM + " text);";

    private SQLiteDatabase db;

    /**
     * Contructor
     * @param context
     */
    public DbManager(Context context) {
        DbHelper helper = new DbHelper(context);
        db= helper.getWritableDatabase();
    }

    /**
     * Generamos los valores de cada columna
     * @param titulo
     * @param fecha
     * @param popularidad
     * @param poster
     * @param posterCache
     * @param fondo
     * @param ownTitle
     * @param director
     * @param casting
     * @param duration
     * @param album
     * @return
     */
    public ContentValues generarContendor(String titulo, String fecha, String popularidad, String poster, String posterCache, String fondo, String ownTitle, String director, String casting, String duration, String album){

        ContentValues valores = new ContentValues();
        valores.put(CN_TITLE, titulo);
        valores.put(CN_DATE, fecha);
        valores.put(CN_POPULARITY, popularidad);
        valores.put(CN_POSTER, poster);
        valores.put(CN_POSTER_CACHE, posterCache);
        valores.put(CN_BACKDROP, fondo);
        valores.put(CN_OWNTITLE, ownTitle);
        valores.put(CN_DIRECTOR, director);
        valores.put(CN_CASTING, casting);
        valores.put(CN_DURATION, duration);
        valores.put(CN_ALBUM, album);

        return valores;
    }

    /**
     * Insertar en la base de datos
     * @param titulo
     * @param fecha
     * @param popularidad
     * @param poster
     * @param posterCache
     * @param fondo
     * @param ownTitle
     * @param director
     * @param casting
     * @param duration
     * @param album
     */
    public void insertDB(String titulo, String fecha, String popularidad, String poster, String posterCache, String fondo, String ownTitle, String director, String casting, String duration, String album){
        db.insert(TABLE_NAME, null, generarContendor(titulo, fecha, popularidad, poster, posterCache, fondo, ownTitle, director, casting, duration, album));
    }

    /**
     * Borramos la entrada de la base de datos
     * @param titulo
     */
    public void deleteDB(String titulo){
        db.delete(TABLE_NAME, CN_TITLE+"=?", new String[] {titulo});
    }

    /**
     * Actualizamos la fila correspondiente
     * @param id
     * @param valores
     */
    public void updateDB(int id, ContentValues valores){
        db.update(TABLE_NAME, valores, CN_ID + "=" + id, null);
    }

    /**
     * Cargamos todas las peliculas
     * @return
     */
    public Cursor cargarPeliculas(){
        String[] columnas= new String[]{CN_ID, CN_TITLE, CN_DATE, CN_POPULARITY, CN_POSTER, CN_POSTER_CACHE, CN_BACKDROP, CN_OWNTITLE, CN_DIRECTOR, CN_CASTING, CN_DURATION, CN_ALBUM};
        return db.query(TABLE_NAME, columnas, null, null, null, null, null);
    }

    /**
     * Cargamos la ultima pelicula insertada
     * @param titulo
     * @return
     */
    public Cursor buscarPeliculaSingle(String titulo) {
        String[] columnas = new String[]{CN_ID, CN_TITLE, CN_DATE, CN_POPULARITY, CN_POSTER, CN_POSTER_CACHE, CN_BACKDROP, CN_OWNTITLE, CN_DIRECTOR, CN_CASTING, CN_DURATION, CN_ALBUM};
        return db.query(TABLE_NAME, columnas, CN_TITLE + " =? ", new String[]{titulo}, null, null, null);
    }

    /**
     * Cargamos las peliculas que coincidan en titulo
     * @param titulo
     * @return
     */
    public Cursor buscarPorTitulo(String titulo) {
        String[] columnas = new String[]{CN_ID, CN_TITLE, CN_DATE, CN_POPULARITY, CN_POSTER, CN_POSTER_CACHE, CN_BACKDROP, CN_OWNTITLE, CN_DIRECTOR, CN_CASTING, CN_DURATION, CN_ALBUM};
        return db.query(TABLE_NAME,columnas,CN_TITLE + " LIKE '%' || ? || '%' ",new String[]{titulo},null,null,null);
    }

    /**
     * Cargamos las peliculas que coinciden con la fecha
     * @param fecha
     * @return
     */
    public Cursor buscarPorFecha(String fecha){
        String[] columnas = new String[]{CN_ID, CN_TITLE, CN_DATE, CN_POPULARITY, CN_POSTER, CN_POSTER_CACHE, CN_BACKDROP, CN_OWNTITLE, CN_DIRECTOR, CN_CASTING, CN_DURATION, CN_ALBUM};
        return db.query(TABLE_NAME, columnas, CN_DATE + " LIKE '%' || ? || '%' ", new String[]{fecha}, null, null, null);
    }

    /**
     * Cargamos las peliculas que coinciden con la valoracion
     * @param valoracion
     * @return
     */
    public Cursor buscarPorValoracion(String valoracion){
        String[] columnas = new String[]{CN_ID, CN_TITLE, CN_DATE, CN_POPULARITY, CN_POSTER, CN_POSTER_CACHE, CN_BACKDROP, CN_OWNTITLE, CN_DIRECTOR, CN_CASTING, CN_DURATION, CN_ALBUM};
        return db.query(TABLE_NAME, columnas, CN_POPULARITY + " LIKE '%' || ? || '%' ", new String[]{valoracion}, null, null, null);
    }

    /**
     * Cargamos las peliculas que coinciden con el titulo personal
     * @param titulo
     * @return
     */
    public Cursor buscarPorTituloPersonal(String titulo){
        String[] columnas = new String[]{CN_ID, CN_TITLE, CN_DATE, CN_POPULARITY, CN_POSTER, CN_POSTER_CACHE, CN_BACKDROP, CN_OWNTITLE, CN_DIRECTOR, CN_CASTING, CN_DURATION, CN_ALBUM};
        return db.query(TABLE_NAME, columnas, CN_OWNTITLE + " LIKE '%' || ? || '%' ", new String[]{titulo}, null, null, null);
    }

    /**
     * Cargamos las peliculas que coinciden con el director
     * @param director
     * @return
     */
    public Cursor buscarPorDirector(String director){
        String[] columnas = new String[]{CN_ID, CN_TITLE, CN_DATE, CN_POPULARITY, CN_POSTER, CN_POSTER_CACHE, CN_BACKDROP, CN_OWNTITLE, CN_DIRECTOR, CN_CASTING, CN_DURATION, CN_ALBUM};
        return db.query(TABLE_NAME, columnas, CN_DIRECTOR + " LIKE '%' || ? || '%' ", new String[]{director}, null, null, null);
    }

    /**
     * Cargamos las peliculas que coinciden con el reparto
     * @param reparto
     * @return
     */
    public Cursor buscarPorReparto(String reparto){
        String[] columnas = new String[]{CN_ID, CN_TITLE, CN_DATE, CN_POPULARITY, CN_POSTER, CN_POSTER_CACHE, CN_BACKDROP, CN_OWNTITLE, CN_DIRECTOR, CN_CASTING, CN_DURATION, CN_ALBUM};
        return db.query(TABLE_NAME, columnas, CN_CASTING + " LIKE '%' || ? || '%' ", new String[]{reparto}, null, null, null);
    }

    /**
     * Cargamos las peliculas que coinciden con el reparto
     * @param album
     * @return
     */
    public Cursor buscarPorAlbum(String album){
        String[] columnas = new String[]{CN_ID, CN_TITLE, CN_DATE, CN_POPULARITY, CN_POSTER, CN_POSTER_CACHE, CN_BACKDROP, CN_OWNTITLE, CN_DIRECTOR, CN_CASTING, CN_DURATION, CN_ALBUM};
        return db.query(TABLE_NAME, columnas, CN_ALBUM + " LIKE '%' || ? || '%' ", new String[]{album}, null, null, null);
    }
}

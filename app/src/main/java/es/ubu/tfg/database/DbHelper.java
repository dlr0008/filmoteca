package es.ubu.tfg.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Clase necesaria de apoyo al manejo de la base de datos
 */
public class DbHelper extends SQLiteOpenHelper {

    private static final String DB_NAME="Movie.sqlite";
    private static final int DB_SCHEME_VERSION=1;

    /**
     * Contructor
     * @param context contexto de la aplicacion
     */
    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_SCHEME_VERSION);
    }

    /**
     * Creador de la base de datos
     * @param db base de datos
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(DbManager.CREATE_TABLE);
        System.out.println(DbManager.CREATE_TABLE);
    }

    /**
     * Actualizador de la base de datos
     * @param db base de datos
     * @param oldVersion version antigua
     * @param newVersion nueva version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

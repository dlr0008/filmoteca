package es.ubu.tfg.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import es.ubu.tfg.filmoteca.R;
import es.ubu.tfg.support.SimpleCursorAdapterLibrary;


public class Explore extends ActionBarActivity {

    private EditText buscar;
    private Cursor cursor;
    int opcion;
    String opcionText, opcionUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore);

        buscar=(EditText) findViewById(R.id.editExplore1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.film_filter, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                opcion = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                opcion = 0;
            }

        });
    }

    public void buscarPelicula(View view){
        switch (opcion) {
            case 0:  cursor=MainActivity.manager.buscarPorTituloPersonal(buscar.getText().toString());
                if(cursor.getCount()==0) {
                    showDiag();
                }else{
                    opcionText = "ownTitle";
                    cursor.moveToFirst();
                    opcionUser = cursor.getString(7);
                }
                break;
            case 1:  cursor=MainActivity.manager.buscarPorTitulo(buscar.getText().toString());
                if(cursor.getCount()==0) {
                    showDiag();
                }else {
                    opcionText = "titulo";
                    cursor.moveToFirst();
                    opcionUser = cursor.getString(1);
                }
                break;
            case 2:  cursor=MainActivity.manager.buscarPorFecha(buscar.getText().toString());
                if(cursor.getCount()==0) {
                    showDiag();
                }else {
                    opcionText = "fecha";
                    cursor.moveToFirst();
                    opcionUser = cursor.getString(2);
                }
                break;
            case 3:  cursor=MainActivity.manager.buscarPorValoracion(buscar.getText().toString());
                if(cursor.getCount()==0) {
                    showDiag();
                }else {
                    opcionText = "popularidad";
                    cursor.moveToFirst();
                    opcionUser = cursor.getString(3);
                }
                break;
            case 4:  cursor=MainActivity.manager.buscarPorDirector(buscar.getText().toString());
                if(cursor.getCount()==0) {
                    showDiag();
                }else {
                    opcionText = "director";
                    cursor.moveToFirst();
                    opcionUser = cursor.getString(8);
                }
                break;
            case 5:  cursor=MainActivity.manager.buscarPorReparto(buscar.getText().toString());
                if(cursor.getCount()==0) {
                    showDiag();
                }else {
                    opcionText = "casting";
                    cursor.moveToFirst();
                    opcionUser = cursor.getString(9);
                }
                break;
            case 6:  cursor=MainActivity.manager.buscarPorAlbum(buscar.getText().toString());
                if(cursor.getCount()==0) {
                    showDiag();
                }else {
                    opcionText = "album";
                    cursor.moveToFirst();
                    opcionUser = cursor.getString(11);
                }
                break;
        }

        ListView lista = (ListView) findViewById(R.id.listExplore1);
        String[] from=new String[]{MainActivity.manager.CN_OWNTITLE, MainActivity.manager.CN_POSTER_CACHE};
        int[] to=new int[]{R.id.textList1, R.id.imageList1};
        SimpleCursorAdapterLibrary adaptador = new SimpleCursorAdapterLibrary(this, R.layout.library_list_item, cursor, from, to, 0);
        lista.setAdapter(adaptador);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                Intent intent = new Intent(getBaseContext(), MovieSummaryLib.class);

                String ident = cursor.getString(0);
                intent.putExtra("Identificador", ident);

                String tituloOriginal = cursor.getString(1);
                intent.putExtra("Original", tituloOriginal);

                String fecha = cursor.getString(2);
                intent.putExtra("Fecha", fecha);

                String posterCache=cursor.getString(5);
                intent.putExtra("PosterCache", posterCache);

                String opcion=opcionText;
                intent.putExtra("OpcionText", opcion);

                String opcionU=opcionUser;
                intent.putExtra("OpcionUser", opcionU);

                view.getContext().startActivity(intent);
            }
        });
    }

    public void showDiag(){
        Dialog dialog;
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.systemMsg3))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        dialog = builder.create();
        dialog.show();
    }
}

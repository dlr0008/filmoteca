package es.ubu.tfg.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;
import java.util.ArrayList;

import es.ubu.tfg.filmoteca.R;


public class MovieSummary extends ActionBarActivity {

    static ImageLoader imageLoader, imageLoaderNoCache;
    ImageView imagen;
    File cache;
    String cacheFile;
    String titulo, tituloOriginal, fecha, popularidad, poster, backdrop, atr1;
    EditText editAtributtes1;
    AutoCompleteTextView editAtributtes2, editAtributtes3, editAtributtes4, editAtributtes5;
    boolean guardado=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_summary);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();

        Context context=getApplicationContext();
        File dataDir=new File(context.getFilesDir().getAbsolutePath());
        File appCacheDir=new File(dataDir,"images");
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .diskCache(new UnlimitedDiscCache(appCacheDir))
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator())
                .denyCacheImageMultipleSizesInMemory()
                .build();
        imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);

        poster=i.getExtras().getString("Poster","");
        imagen=(ImageView)findViewById(R.id.imageSumm1);
        imageLoader.displayImage("http://cf2.imgobject.com/t/p/original" + poster, imagen, options);

        tituloOriginal = i.getExtras().getString("Original","");
        TextView textView4 = (TextView)findViewById(R.id.textSumm4);
        textView4.setText(tituloOriginal);

        fecha = i.getExtras().getString("Fecha","");
        TextView textView6 = (TextView)findViewById(R.id.textSumm6);
        textView6.setText(fecha);

        cache = imageLoader.getDiskCache().get("http://cf2.imgobject.com/t/p/original" + poster);
        cacheFile = cache.toString();

        Cursor peliculas = MainActivity.manager.cargarPeliculas();

        //Rellenamos los String de sugerencias
        ArrayList<String> directores = new ArrayList<>();
        ArrayList<String> reparto = new ArrayList<>();
        ArrayList<String> album = new ArrayList<>();
        ArrayList<String> duracion = new ArrayList<>();

        peliculas.moveToFirst();
        int q=0;
        for(int c=0;c<peliculas.getCount();c++){
            if(peliculas.getString(8)!=null && !directores.contains(peliculas.getString(8))){
                directores.add(q, peliculas.getString(8));
                q+=1;
            }
            peliculas.moveToNext();
        }

        peliculas.moveToFirst();
        int w=0;
        for(int c=0;c<peliculas.getCount();c++){
            if(peliculas.getString(9)!=null && !reparto.contains(peliculas.getString(9))){
                reparto.add(w, peliculas.getString(9));
                w+=1;
            }
            peliculas.moveToNext();
        }

        peliculas.moveToFirst();
        int z=0;
        for(int c=0;c<peliculas.getCount();c++){
            if(peliculas.getString(11)!=null && !album.contains(peliculas.getString(11))){
                album.add(z, peliculas.getString(11));
                z+=1;
            }
            peliculas.moveToNext();
        }

        peliculas.moveToFirst();
        int b=0;
        for(int c=0;c<peliculas.getCount();c++){
            if(peliculas.getString(10)!=null && !album.contains(peliculas.getString(10))){
                duracion.add(b, peliculas.getString(10));
                b+=1;
            }
            peliculas.moveToNext();
        }

        String[] directores1 = directores.toArray(new String[directores.size()]);
        String[] reparto1 = reparto.toArray(new String[reparto.size()]);
        String[] album1 = album.toArray(new String[album.size()]);
        String[] duracion1 = duracion.toArray(new String[duracion.size()]);

        editAtributtes1 = (EditText) findViewById(R.id.editAtributtes1);

        editAtributtes2 = (AutoCompleteTextView) findViewById(R.id.editAtributtes2);
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,directores1);
        editAtributtes2.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        editAtributtes3 = (AutoCompleteTextView) findViewById(R.id.editAtributtes3);
        ArrayAdapter adapter2 = new ArrayAdapter(this,android.R.layout.simple_list_item_1,album1);
        editAtributtes3.setAdapter(adapter2);
        adapter2.notifyDataSetChanged();

        editAtributtes4 = (AutoCompleteTextView) findViewById(R.id.editAtributtes4);
        ArrayAdapter adapter3 = new ArrayAdapter(this,android.R.layout.simple_list_item_1,reparto1);
        editAtributtes4.setAdapter(adapter3);
        adapter3.notifyDataSetChanged();

        editAtributtes5=(AutoCompleteTextView) findViewById(R.id.editAtributtes5);
        ArrayAdapter adapter4 = new ArrayAdapter(this,android.R.layout.simple_list_item_1,duracion1);
        editAtributtes5.setAdapter(adapter4);
        adapter4.notifyDataSetChanged();

        atr1 = i.getExtras().getString("ownTitle","");
        editAtributtes1.setText(atr1);

        if(isRowDuplicate(tituloOriginal)){
            guardado=true;
        }
    }

    public void irAFilmaffinity(View v){
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.filmaffinity.com/es/search.php?stext=" + tituloOriginal)));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_movie_summary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id==android.R.id.home){
            NavUtils.navigateUpFromSameTask(this);
        }
        if (id == R.id.save) {
            if(imagen.getDrawable() == null){
                Dialog dialog;
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.systemMsg5))
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                dialog = builder.create();
                dialog.show();
                guardado = true;
            }else {
                if (!isRowDuplicate(tituloOriginal)) {

                    atr1 = String.valueOf(editAtributtes1.getText());
                    String atr2 = String.valueOf(editAtributtes2.getText());
                    String atr3 = String.valueOf(editAtributtes3.getText());
                    String atr4 = String.valueOf(editAtributtes4.getText());
                    String atr5 = String.valueOf(editAtributtes5.getText());

                    MainActivity.manager.insertDB(tituloOriginal, fecha, "0", poster, cacheFile, backdrop, atr1, atr2, atr4, atr5, atr3);
                    Toast.makeText(getApplicationContext(), getString(R.string.systemMsg6), Toast.LENGTH_SHORT).show();
                    guardado = true;
                } else {
                    Dialog dialog;
                    AlertDialog.Builder builder;
                    builder = new AlertDialog.Builder(this);
                    builder.setMessage(getString(R.string.systemMsg7))
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                    dialog = builder.create();
                    dialog.show();
                    guardado = true;
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Comprobamos que ya tenemos la pelicula guardadq
     * @param title titulo de la pelicula
     * @return falso si no la tenemos, verdadero al contrario
     */
    private boolean isRowDuplicate(String title) {
        Cursor cursor=MainActivity.manager.cargarPeliculas();
        cursor.moveToFirst();
        if(cursor.getCount()==0){
            return false;
        }
        if(title.equals(cursor.getString(1))){
            return true;
        }
        while(cursor.moveToNext()){
            if(title.equals(cursor.getString(1))){
                return true;
            }
        }
        return false;
    }

    /**
     * Si no hemos guardado la pelicula, la imagen en cache es borrada
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if(!guardado){
            File file = new File(cacheFile);
            file.delete();
        }
    }
}

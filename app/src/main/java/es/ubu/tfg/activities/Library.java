package es.ubu.tfg.activities;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import es.ubu.tfg.filmoteca.R;
import es.ubu.tfg.support.SimpleCursorAdapterLibrary;


public class Library extends ActionBarActivity {

    private Cursor cursor;
    SimpleCursorAdapterLibrary adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cursor=MainActivity.manager.cargarPeliculas();
        final ListView lista = (ListView) findViewById(R.id.listLibrary1);
        String[] from=new String[]{MainActivity.manager.CN_OWNTITLE, MainActivity.manager.CN_POSTER_CACHE};
        int[] to=new int[]{R.id.textList1, R.id.imageList1};
        adaptador = new SimpleCursorAdapterLibrary(this, R.layout.library_list_item, cursor, from, to, 0);
        lista.setAdapter(adaptador);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                Intent intent = new Intent(getBaseContext(), MovieSummaryLib.class);

                cursor.moveToPosition(position);

                String ident = cursor.getString(0);
                intent.putExtra("Identificador", ident);

                String tituloOriginal = cursor.getString(1);
                intent.putExtra("Original", tituloOriginal);

                String fecha = cursor.getString(2);
                intent.putExtra("Fecha", fecha);

                String posterCache=cursor.getString(5);
                intent.putExtra("PosterCache", posterCache);

                view.getContext().startActivity(intent);
            }
        });
    }
}

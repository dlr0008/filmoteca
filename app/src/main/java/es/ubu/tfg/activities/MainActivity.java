package es.ubu.tfg.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashMap;

import es.ubu.tfg.database.DbManager;
import es.ubu.tfg.filmoteca.R;
import es.ubu.tfg.support.NavigationDrawerFragment;
import es.ubu.tfg.support.SimpleCursorAdapterLibrary;


public class MainActivity extends ActionBarActivity {

    public static DbManager manager;
    private Cursor cursor;
    int count;
    public static HashMap<String, String> idPeliculas;
    ImageView img1, img2, img3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer,(DrawerLayout)findViewById(R.id.drawer_layout), toolbar);

        img1=(ImageView) findViewById(R.id.imageMain1);
        img1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), Add.class);
                v.getContext().startActivity(myIntent);
            }
        });

        img2=(ImageView) findViewById(R.id.imageMain2);
        img2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), Library.class);
                final ProgressDialog progressDialog;
                progressDialog = new ProgressDialog(v.getContext());
                progressDialog.setMessage(getString(R.string.systemMsg1));
                progressDialog.setCancelable(true);
                progressDialog.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                }).start();
                v.getContext().startActivity(myIntent);
            }
        });

        img3=(ImageView) findViewById(R.id.imageMain3);
        img3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), Explore.class);
                v.getContext().startActivity(myIntent);
            }
        });

        manager=new DbManager(this);

        Cursor cursorCount=manager.cargarPeliculas();
        count=cursorCount.getCount();

        TextView textview=(TextView) findViewById(R.id.textStart6);
        textview.setText(""+count);

        cursorCount.moveToLast();

        if(cursorCount.getCount()!=0) {
            cursor = manager.buscarPeliculaSingle(cursorCount.getString(1));
        }

        ListView lista = (ListView) findViewById(R.id.listLibrary1);
        String[] from=new String[]{MainActivity.manager.CN_OWNTITLE, MainActivity.manager.CN_POSTER_CACHE};
        int[] to=new int[]{R.id.textList1, R.id.imageList1};
        SimpleCursorAdapterLibrary adaptador = new SimpleCursorAdapterLibrary(this, R.layout.library_list_item, cursor, from, to, 0);
        lista.setAdapter(adaptador);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                Intent intent = new Intent(getBaseContext(), MovieSummaryLib.class);

                cursor.moveToPosition(position);

                String ident = cursor.getString(0);
                intent.putExtra("Identificador", ident);

                String tituloOriginal = cursor.getString(1);
                intent.putExtra("Original", tituloOriginal);

                String fecha = cursor.getString(2);
                intent.putExtra("Fecha", fecha);

                String posterCache=cursor.getString(5);
                intent.putExtra("PosterCache", posterCache);

                view.getContext().startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.search) {
            startActivity(new Intent(this, Add.class));
        }

        return super.onOptionsItemSelected(item);
    }
}
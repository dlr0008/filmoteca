package es.ubu.tfg.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Locale;

import es.ubu.tfg.filmoteca.R;


public class Settings extends ActionBarActivity {

    Button button;
    final CharSequence[] items = {"Español", "Inglés", "Francés"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        File direct = new File(Environment.getExternalStorageDirectory() + "/Filmoteca_Backup");
        File direct2 = new File(Environment.getExternalStorageDirectory() + "/Filmoteca_Backup/images");


        if (!direct.exists()) {
            if (direct.mkdir()) {
            }
        }

        if (!direct2.exists()) {
            if (direct2.mkdir()) {
            }
        }
    }


    /**
     * Cambiamos el idioma en funcion de la eleccion del usuario
     *
     * @param view
     */
    public void changeLocale(View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.systemMsg13));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    setLocale("sp");
                } else if (item == 1) {
                    setLocale("en");
                } else if (item == 2) {
                    setLocale("fr");
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Configuramos el "locale" de la app
     *
     * @param lang
     */
    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    /**
     * Reseteamos la base de datos
     *
     * @param view
     */
    public void resetBD(View view) {
        Dialog dialog;
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.systemMsg14))
                .setCancelable(false)
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getApplicationContext().deleteDatabase("Movie.sqlite");
                        File dataDir = new File(getApplicationContext().getFilesDir().getAbsolutePath());
                        if (dataDir.exists()) {
                            String deleteCmd = "rm -r " + dataDir;
                            Runtime runtime = Runtime.getRuntime();
                            try {
                                runtime.exec(deleteCmd);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                });
        dialog = builder.create();
        dialog.show();
    }

    /**
     * Importamos la base de datos
     *
     * @param view
     */
    public void importDB(View view) {
        Dialog dialog;
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.systemMsg15))
                .setCancelable(false)
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                })
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, int id) {
                        try {
                            File sd = Environment.getExternalStorageDirectory();

                            if (sd.canWrite()) {
                                String currentDBPath = getApplicationContext().getDatabasePath("Movie.sqlite").toString();
                                String backupDBPath = "/Filmoteca_Backup/Movie.sqlite";
                                File backupDB = new File(currentDBPath);
                                File currentDB = new File(sd, backupDBPath);

                                FileChannel src = new FileInputStream(currentDB).getChannel();
                                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                                dst.transferFrom(src, 0, src.size());
                                src.close();
                                dst.close();
                                importImages();
                                Intent refresh = new Intent(getApplicationContext(), MainActivity.class);
                                refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(refresh);
                            }
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), getString(R.string.systemMsg16), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        dialog = builder.create();
        dialog.show();
    }

    /**
     * Exportamos la base de datos
     *
     * @param view
     */
    public void exportDB(View view) {

        Cursor cursorCount=MainActivity.manager.cargarPeliculas();
        int count=cursorCount.getCount();
        if(count != 0) {
            try {
                File sd = Environment.getExternalStorageDirectory();

                if (sd.canWrite()) {
                    String currentDBPath = getApplicationContext().getDatabasePath("Movie.sqlite").toString();
                    String backupDBPath = "/Filmoteca_Backup/Movie.sqlite";
                    File currentDB = new File(currentDBPath);
                    File backupDB = new File(sd, backupDBPath);

                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                    exportImages();

                    Dialog dialog2;
                    AlertDialog.Builder builder;
                    builder = new AlertDialog.Builder(this);
                    builder.setMessage(getString(R.string.systemMsg17))
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                    dialog2 = builder.create();
                    dialog2.show();
                }
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), getString(R.string.systemMsg18), Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(getApplicationContext(), getString(R.string.systemMsg21), Toast.LENGTH_SHORT).show();
        }
    }

    public void exportImages() {

        try {
            File sd = Environment.getExternalStorageDirectory();

            if (sd.canWrite()) {
                String backupDBPath = "/Filmoteca_Backup/images";
                File dataDir = new File(getApplicationContext().getFilesDir().getAbsolutePath());
                File appCacheDir = new File(dataDir, "images");
                File backupFiles = new File(sd, backupDBPath);
                copyFolder(appCacheDir, backupFiles);
            }
        } catch (Exception e) {
            Dialog dialog2;
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(getApplicationContext());
            builder.setMessage(getString(R.string.systemMsg19))
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            dialog2 = builder.create();
            dialog2.show();
        }
    }

    public void importImages() {

        try {
            File sd = Environment.getExternalStorageDirectory();

            if (sd.canWrite()) {
                String backupDBPath = "/Filmoteca_Backup/images";
                File dataDir = new File(getApplicationContext().getFilesDir().getAbsolutePath());
                File appCacheDir = new File(dataDir, "images");
                File backupFiles = new File(sd, backupDBPath);
                copyFolder(backupFiles, appCacheDir);
            }
        } catch (Exception e) {
            Dialog dialog2;
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(getApplicationContext());
            builder.setMessage(getString(R.string.systemMsg20))
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            dialog2 = builder.create();
            dialog2.show();
        }
    }

    public static void copyFolder(File src, File dest)
            throws IOException {

        if (src.isDirectory()) {
            if (!dest.exists()) {
                dest.mkdir();
            }
            FileUtils.copyDirectory(src, dest);

        }
    }
}

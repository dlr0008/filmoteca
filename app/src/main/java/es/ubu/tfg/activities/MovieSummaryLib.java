package es.ubu.tfg.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;
import java.util.ArrayList;

import es.ubu.tfg.filmoteca.R;


public class MovieSummaryLib extends ActionBarActivity {

    static ImageLoader imageLoader;
    ImageView imagen;
    String editSave1, editSave2, editSave3, editSave4, editSave5, ident, tituloOriginal, fecha, poster, posterCache, backdrop, popularity, opcion, opcionU;
    SharedPreferences.Editor editor;
    EditText editAtributtes1;
    AutoCompleteTextView editAtributtes2, editAtributtes3, editAtributtes4, editAtributtes5;
    CheckBox checkBox;
    RatingBar ratingBar;
    int identInt;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_sum_library);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(getApplicationContext()));
        imageLoader = ImageLoader.getInstance();

        Intent i = getIntent();

        ident = i.getExtras().getString("Identificador", "");
        identInt = Integer.parseInt(ident);

        posterCache = i.getExtras().getString("PosterCache", "");

        imagen = (ImageView) findViewById(R.id.imageSumm1);
        imageLoader.displayImage("file:///" + posterCache, imagen);

        tituloOriginal = i.getExtras().getString("Original", "");
        TextView textView8 = (TextView) findViewById(R.id.textSumm4);
        textView8.setText(tituloOriginal);

        fecha = i.getExtras().getString("Fecha", "");
        TextView textView10 = (TextView) findViewById(R.id.textSumm6);
        textView10.setText(fecha);

        opcion = i.getExtras().getString("OpcionText", "");

        opcionU = i.getExtras().getString("OpcionUser", "");

        cursor = MainActivity.manager.cargarPeliculas();

        if (opcion != null) {
            switch (opcion) {
                case "ownTitle":
                    cursor.moveToFirst();
                    while (!cursor.getString(7).equals(opcionU)) {
                        cursor.moveToNext();
                    }
                    break;
                case "titulo":
                    cursor.moveToFirst();
                    while (!cursor.getString(1).equals(opcionU)) {
                        cursor.moveToNext();
                    }
                    break;
                case "fecha":
                    cursor.moveToFirst();
                    while (!cursor.getString(2).equals(opcionU)) {
                        cursor.moveToNext();
                    }
                    break;
                case "popularidad":
                    cursor.moveToFirst();
                    while (!cursor.getString(3).equals(opcionU)) {
                        cursor.moveToNext();
                    }
                    break;
                case "director":
                    cursor.moveToFirst();
                    while (!cursor.getString(8).equals(opcionU)) {
                        cursor.moveToNext();
                    }
                    break;
                case "casting":
                    cursor.moveToFirst();
                    while (!cursor.getString(9).equals(opcionU)) {
                        cursor.moveToNext();
                    }
                    break;
                case "album":
                    cursor.moveToFirst();
                    while (!cursor.getString(11).equals(opcionU)) {
                        cursor.moveToNext();
                    }
                    break;
            }
        }


        cursor.moveToFirst();
        while (!cursor.getString(1).equals(tituloOriginal)) {
            cursor.moveToNext();
        }

        System.out.println(cursor.getString(7));
        popularity = cursor.getString(3);

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);

        if (popularity != null) {
            ratingBar.setRating(Float.parseFloat(popularity));
        }

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                String stars = String.valueOf(rating);
                ContentValues valores = new ContentValues();
                valores.put("popularity", rating);
                MainActivity.manager.updateDB(identInt, valores);
            }
        });

        Cursor peliculas = MainActivity.manager.cargarPeliculas();

        ArrayList<String> directores = new ArrayList<>();
        ArrayList<String> reparto = new ArrayList<>();
        ArrayList<String> album = new ArrayList<>();
        ArrayList<String> duracion = new ArrayList<>();

        peliculas.moveToFirst();
        int q = 0;
        for (int c = 0; c < peliculas.getCount(); c++) {
            if (peliculas.getString(8) != null && !directores.contains(peliculas.getString(8))) {
                directores.add(q, peliculas.getString(8));
                q += 1;
            }
            peliculas.moveToNext();
        }

        peliculas.moveToFirst();
        int w = 0;
        for (int c = 0; c < peliculas.getCount(); c++) {
            if (peliculas.getString(9) != null && !reparto.contains(peliculas.getString(9))) {
                reparto.add(w, peliculas.getString(11));
                w += 1;
            }
            peliculas.moveToNext();
        }

        peliculas.moveToFirst();
        int z = 0;
        for (int c = 0; c < peliculas.getCount(); c++) {
            if (peliculas.getString(11) != null && !album.contains(peliculas.getString(11))) {
                album.add(z, peliculas.getString(9));
                z += 1;
            }
            peliculas.moveToNext();
        }

        peliculas.moveToFirst();
        int b = 0;
        for (int c = 0; c < peliculas.getCount(); c++) {
            if (peliculas.getString(10) != null && !album.contains(peliculas.getString(10))) {
                duracion.add(b, peliculas.getString(10));
                b += 1;
            }
            peliculas.moveToNext();
        }

        String[] directores1 = directores.toArray(new String[directores.size()]);
        String[] reparto1 = reparto.toArray(new String[reparto.size()]);
        String[] album1 = album.toArray(new String[album.size()]);
        String[] duracion1 = duracion.toArray(new String[duracion.size()]);


        editAtributtes1 = (EditText) findViewById(R.id.editAtributtes1);
        editAtributtes1.setText(cursor.getString(7));


        editAtributtes2 = (AutoCompleteTextView) findViewById(R.id.editAtributtes2);
        editAtributtes2.setText(cursor.getString(8));
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, directores1);
        editAtributtes2.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        editAtributtes3 = (AutoCompleteTextView) findViewById(R.id.editAtributtes3);
        editAtributtes3.setText(cursor.getString(11));
        ArrayAdapter adapter2 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, reparto1);
        editAtributtes3.setAdapter(adapter2);
        adapter2.notifyDataSetChanged();

        editAtributtes4 = (AutoCompleteTextView) findViewById(R.id.editAtributtes4);
        editAtributtes4.setText(cursor.getString(9));
        ArrayAdapter adapter3 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, album1);
        editAtributtes4.setAdapter(adapter3);
        adapter3.notifyDataSetChanged();

        editAtributtes5 = (AutoCompleteTextView) findViewById(R.id.editAtributtes5);
        editAtributtes5.setText(cursor.getString(10));
        ArrayAdapter adapter4 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, duracion1);
        editAtributtes5.setAdapter(adapter4);
        adapter4.notifyDataSetChanged();
    }

    public void irAFilmaffinity(View v) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.filmaffinity.com/es/search.php?stext=" + tituloOriginal)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_movie_sum_library, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        if (id == R.id.delete) {
            Dialog dialog;
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.systemMsg8))
                    .setCancelable(false)
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    })
                    .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            File file = new File(posterCache);
                            file.delete();
                            MainActivity.manager.deleteDB(tituloOriginal);
                            Toast.makeText(getApplicationContext(), getString(R.string.systemMsg9), Toast.LENGTH_SHORT).show();
                            Intent refresh = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(refresh);
                        }
                    });
            dialog = builder.create();
            dialog.show();
        }
        if (id == R.id.saveLib) {
            editSave1 = editAtributtes1.getText().toString();
            editSave2 = editAtributtes2.getText().toString();
            editSave3 = editAtributtes3.getText().toString();
            editSave4 = editAtributtes4.getText().toString();
            editSave5 = editAtributtes5.getText().toString();

            ContentValues valores = new ContentValues();
            valores.put("ownTitle", editSave1);
            valores.put("director", editSave2);
            valores.put("casting", editSave4);
            valores.put("duration", editSave5);
            valores.put("album", editSave3);

            MainActivity.manager.updateDB(identInt, valores);
            Toast.makeText(getApplicationContext(), getString(R.string.systemMsg6), Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }
}

package es.ubu.tfg.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import es.ubu.tfg.support.ArrayAdapterAdd;
import es.ubu.tfg.support.MovieResult;
import es.ubu.tfg.filmoteca.R;


public class Add extends ActionBarActivity {

    public static final String EXTRA_QUERY = "lsi.ubu.filmoteca.QUERY";
    ProgressDialog pDialog;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    /**
     * Chequeamos la conexion y extraemos el texto del campo de busqueda
     *
     * @param view necesario para el layout
     */
    public void checkConnection(View view) {

        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getString(R.string.systemMsg1));
        pDialog.setCancelable(true);
        pDialog.show();

        Intent intent = getIntent();
        editText = (EditText) findViewById(R.id.editAdd1);
        String query = null;

        try {
            query = URLEncoder.encode(editText.getText().toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        intent.putExtra(EXTRA_QUERY, query);

        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new TMDBQueryManager().execute(query);
        } else {
            Dialog dialog;
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.systemMsg2))
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            dialog = builder.create();
            dialog.show();
        }
    }

    /**
     * Actualizamos la lista con los resultados.
     *
     * @param result resultado de la busqueda
     */
    public void updateViewWithResults(final ArrayList<MovieResult> result) {
        final ListView lista = (ListView) findViewById(R.id.listAdd1);

        if(result!=null) {
            ArrayAdapterAdd<MovieResult> adapter =
                    new ArrayAdapterAdd<>(this,
                            R.layout.movie_list_item, result);
            lista.setAdapter(adapter);

            if (lista.getCount() == 0) {
                Dialog dialog;
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.systemMsg3))
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                dialog = builder.create();
                dialog.show();
            }
            pDialog.dismiss();

            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                    Intent intent = new Intent(getBaseContext(), MovieSummary.class);

                    String titulo = result.get(position).getTitle();
                    intent.putExtra("Titulo", titulo);

                    String poster = result.get(position).getPosterPath();
                    intent.putExtra("Poster", poster);

                    String tituloOriginal = result.get(position).getOriginalTitle();
                    intent.putExtra("Original", tituloOriginal);

                    String fecha = result.get(position).getReleaseDate();
                    intent.putExtra("Fecha", fecha);

                    String ownTitle = String.valueOf(editText.getText());
                    intent.putExtra("ownTitle", ownTitle);

                    view.getContext().startActivity(intent);
                }
            });
        }else{
            Dialog dialog;
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.systemMsg4))
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            dialog = builder.create();
            dialog.show();
        }
    }


    private class TMDBQueryManager extends AsyncTask {

        private final String TMDB_API_KEY = "89f5c17647b7e84fe3286cf488f3e128";

        /**
         * Pedimos los resultados en segundo plano
         * @param params
         * @return
         */
        @Override
        protected ArrayList<MovieResult> doInBackground(Object... params) {
            try {
                return searchIMDB((String) params[0]);
            } catch (IOException e) {
                return null;
            }
        }

        /**
         * Una vez tengamos los resultados, actualizamos la lista
         * @param result
         */
        @Override
        protected void onPostExecute(Object result) {
            updateViewWithResults((ArrayList<MovieResult>) result);
        }


        /**
         * Montamos la conexion, pedimos el archivo JSON con el resultado y lo parseamos
         * @param query
         * @return
         * @throws IOException
         */
        public ArrayList<MovieResult> searchIMDB(String query) throws IOException {
            // Build URL
            URL url = new URL("http://api.themoviedb.org/3/search/movie" + "?api_key=" + TMDB_API_KEY + "&query=" + query);

            InputStream stream = null;
            try {
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.addRequestProperty("Accept", "application/json");
                conn.setDoInput(true);
                conn.connect();

                stream = conn.getInputStream();
                return parseResult(stringify(stream));
            } finally {
                if (stream != null) {
                    stream.close();
                }
            }
        }

        /**
         * Parser del archivo JSON
         * @param result Objetivo que necesitamos parsear
         * @return resultado parseado
         */
        private ArrayList<MovieResult> parseResult(String result) {
            ArrayList<MovieResult> results = new ArrayList<>();
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray array = (JSONArray) jsonObject.get("results");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonMovieObject = array.getJSONObject(i);
                    MovieResult.Builder movieBuilder = MovieResult.newBuilder(
                            Integer.parseInt(jsonMovieObject.getString("id")),
                            jsonMovieObject.getString("title"))
                            .setBackdropPath(jsonMovieObject.getString("backdrop_path"))
                            .setOriginalTitle(jsonMovieObject.getString("original_title"))
                            .setPopularity(jsonMovieObject.getString("popularity"))
                            .setPosterPath(jsonMovieObject.getString("poster_path"))
                            .setReleaseDate(jsonMovieObject.getString("release_date"));
                    results.add(movieBuilder.build());
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return results;
        }

        /**
         * Clase de apoyo al parser
         * @param stream
         * @return
         * @throws IOException
         */
        public String stringify(InputStream stream) throws IOException{
            Reader reader;
            reader = new InputStreamReader(stream, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(reader);
            return bufferedReader.readLine();
        }
    }

    @Override
    public void onBackPressed() {
        NavUtils.navigateUpFromSameTask(this);
    }
}
